# GameEngine
A simple game engine used to develop games for the __101 FLOSS games__
project

GameEngine is usually included as a submodule in the target game project repository.

## Including in a project
- clone into the target project directory as a submodule:  
`git submodule add https://gitlab.com/101_floss_games/gameengine.git`
- add the newly created git submodule file and directory:  
`git add .gitmodules gameengine`

## Building from within a project (e.x. *mygame*)
- include the submodule into *mygame's* CMakeLists.txt:  

```
project(mygame)
...
# include gameengine into this project
include("${PROJECT_SOURCE_DIR}/gameengine/CMakeLists.txt")
...
add_executable(mygame src1.cpp src2.cpp)
# link gameengine with the current target
target_link_libraries(gameengine mygame)
```
- cmake ../path/to/mygame/src
- make

# Copyright
Part of the __101 FLOSS games project__

*2022 Mohammad Rasmi Khashashneh* 

## License
```
GameEngine

The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
he rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
```
