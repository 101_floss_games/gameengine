/**
* renderer_sdl.cpp
*
* GameEngine
*
* 2022 Mohammad Rasmi Khashashneh
*
* MIT license
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#include "renderer_sdl.hpp"

#include <iostream>
#include <string>
#include <stdexcept>

namespace gameengine {
	RendererSDL::RendererSDL(const WindowManagerSDL *windowManager) :
		windowManager(windowManager), renderer(NULL) {
		std::cout << "RendererSDL constructor" << std::endl;
	}

	void RendererSDL::doInitialization() {
		std::cout << "RendererSDL doInitialization" << std::endl;

		if(!(SDL_INIT_VIDEO & SDL_WasInit(SDL_INIT_VIDEO))) {
			throw std::logic_error(
				"RendererSDL: Must initialize vidoe subsystem first"
			);
		}

		const SDL_Window * window = windowManager->getWindow();
		if(window == NULL) {
			throw std::logic_error(
				"RendererSDL: Must initialize window first!"
			);
		}

		renderer = SDL_CreateRenderer(
			const_cast<SDL_Window *>(window),
			-1,
			SDL_RENDERER_ACCELERATED
		);

		if(renderer == NULL) {
			throw std::logic_error(
				"RendererSDL: create rednerer error =  "
				+ std::string(SDL_GetError())
			);
		}

		//TODO: add as a windowproperty property
		SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
	}

	void RendererSDL::doCleanup() {
		std::cout << "RendererSDL::doCleanup" << std::endl;
		if(SDL_INIT_VIDEO & SDL_WasInit(SDL_INIT_VIDEO))
			SDL_QuitSubSystem(SDL_INIT_VIDEO);
	}

	void RendererSDL::render() {
		SDL_RenderClear(renderer);
		SDL_RenderPresent(renderer);
	}
}
