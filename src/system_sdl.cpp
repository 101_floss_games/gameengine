/**
* system_sdl.cpp
*
* GameEngine
*
* 2022 Mohammad Rasmi Khashashneh
*
* MIT license
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#include "system_sdl.hpp"
#include "renderer_sdl.hpp"
#include "windowmanager_sdl.hpp"
#include "input_sdl.hpp"
#include <iostream>

namespace gameengine {

	SystemSDL::~SystemSDL() {
		std::cout << "SystemSDL destructor" << std::endl;
	}

	bool SystemSDL::initSystem() {
		try {
			if(SDL_WasInit(0))
				return false;

			SDL_SetMainReady();

			//NOTE: consider initializing all subsystems instead of individualy
			initSubSystems();

			WindowManagerSDL *windowManagerSdl = new WindowManagerSDL();
			windowManager = windowManagerSdl;
			renderer = new RendererSDL(windowManagerSdl);
			input = new InputSDL();

			return true;
		} catch(const std::exception &ex) {
			throw std::runtime_error(
				"SystemSDL ERROR: init error " + std::string(ex.what())
			);
		}
	}

	void SystemSDL::initSubSystems() {
		int result = SDL_InitSubSystem(SDL_INIT_VIDEO);
		if(0 != result) {
			throw std::runtime_error(
				"SystemSDL: init subsystem error= " + std::to_string(result)
			);
		}
	}

	void SystemSDL::freeSystem() {
		SDL_Quit();
		delete renderer;
	}

	bool SystemSDL::initRenderer() {
		try {
			renderer->init();
			return true;
		} catch(const std::exception &ex) {
			std::cout << "SystemSDL ERROR: " << ex.what() << std::endl;
			return false;
		}
	}

	void SystemSDL::freeRenderer() {
		renderer->free();
	}


	bool SystemSDL::initInput() {
		try {
			input->init();
			return true;
		} catch(const std::exception &ex) {
			std::cout << "SystemSDL ERROR: " << ex.what() << std::endl;
			return false;
		}
	}

	void SystemSDL::freeInput() {
		input->free();
	}

	bool SystemSDL::initWindowManager(
		const WindowProperties &windowProperties
	) {
		try {
			windowManager->init(windowProperties);
			return true;
		} catch(const std::exception &ex) {
			std::cout << "SystemSDL ERROR: " << ex.what() << std::endl;
			return false;
		}
	}

	void SystemSDL::freeWindowManager() {
		windowManager->close();
	}

	void SystemSDL::renderScene(const Scene * const scene) const {
		renderer->render();
	}
}
