/**
* system_generic.cpp
*
* GameEngine
*
* 2022 Mohammad Rasmi Khashashneh
*
* MIT license
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#include "system_generic.hpp"
#include "renderer_generic.hpp"
#include <iostream>

namespace gameengine {

	SystemGeneric::~SystemGeneric() {
		std::cout << "System Generic stub" << std::endl;
	}

	bool SystemGeneric::systemInit() {
		std::cout << "System Generic initialization stub" << std::endl;
		renderer = new RendererGeneric();

		return true;
	}

	void SystemGeneric::systemFree() {
		delete renderer;
	}

	bool SystemGeneric::initRenderer() {
		try {
			renderer->init();
			return true;
		} catch(const std::exception &ex) {
			std::cout << "SystemGeneric ERROR: " << ex.what() << std::endl;
			return false;
		}
	}

	void SystemGeneric::freeRenderer() {
		renderer->free();
	}
}
