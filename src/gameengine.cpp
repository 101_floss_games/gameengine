/**
* gameengine.cpp
*
* GameEngine
*
* 2022 Mohammad Rasmi Khashashneh
*
* MIT license
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#include "gameengine.hpp"
#include <thread>

namespace gameengine {
	GameEngine::GameEngine(
		System* system,
		const WindowProperties &windowProperties) :
		engineState(State::STOPPED),
		system(system), windowProperties(windowProperties) {}

	GameEngine::~GameEngine() {
		delete system;
	}

	bool GameEngine::init(const WindowProperties &windowProperties) {
		return system->init(windowProperties);
	}

	void GameEngine::free() {
		system->free();
	}

	void GameEngine::runState() {
		engineState = State::RUNNING;
		currentScene->init();
		using fps = std::chrono::duration<double, std::ratio<1, 60>>;
		std::chrono::duration<double, std::milli> elapsed;
		const Input &input = system->getInputObject();

		while(engineState == State::RUNNING) {
			auto start = std::chrono::system_clock::now();

			//FIXME: elapsed has something wrong (extra). MUST be fixed
			currentScene->process(elapsed.count());
			currentScene->input(input.getInput());
			engineState =
				currentScene->isExiting() ? State::STOPPED : State::RUNNING;
			system->render(currentScene);

			auto end = std::chrono::system_clock::now();
			std::chrono::duration<double, std::milli>  processing =
				end - start;
			std::chrono::duration<double, std::milli> remaining =
				fps(FPS_60) - processing;

			std::this_thread::sleep_for(remaining);

			elapsed = std::chrono::system_clock::now() - start;
		}
	}

	bool GameEngine::run() {
		if(engineState == State::RUNNING)
			return false;

		try{
			if(!init(windowProperties))
				return false;
			runState();
			//TODO: renderers cannot be invoked outside of their thread.
			//std::thread thread(&GameEngine::runState, this);
			//thread.join();
			free();
			return true;
		} catch(const std::exception &ex) {
			return false;
		}
	}

	void GameEngine::setScene(Scene *scene) {
		currentScene = scene;
	}

	State GameEngine::getState() const {
		return engineState;
	}
}
