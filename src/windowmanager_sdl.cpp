/**
* windowmanager_sdl.cpp
*
* GameEngine
*
* 2022 Mohammad Rasmi Khashashneh
*
* MIT license
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#include "windowmanager_sdl.hpp"
#include <stdexcept>
#include <iostream>

namespace gameengine {

	WindowManagerSDL::WindowManagerSDL() : window(NULL) {
		std::cout << "WindowManagerSDL constructor" << std::endl;
	}

	void WindowManagerSDL::createWindow() {
		if(!(SDL_INIT_VIDEO & SDL_WasInit(SDL_INIT_VIDEO))) {
			throw std::logic_error(
				"WindowManagerSDL: Must initialize video ubsystem first"
			);
		}

		//TODO: for now vulkan, allow to specify or fallback depending on platform
		uint32_t flags = SDL_WINDOW_VULKAN;
		if(windowProperties.fullScreen)
			flags |= SDL_WINDOW_FULLSCREEN;

		int xPos = windowProperties.x == -1 ?
			SDL_WINDOWPOS_UNDEFINED : windowProperties.x;
		int yPos = windowProperties.y == -1 ?
			SDL_WINDOWPOS_UNDEFINED : windowProperties.y;

		window = SDL_CreateWindow(
			windowProperties.title.c_str(),
			xPos,
			yPos,
			windowProperties.width,
			windowProperties.height,
			flags
		);

		if(window == NULL) {
			throw std::runtime_error(
				"WindowManagerSDL: create window error = "
				+ std::string(SDL_GetError())
			);
		}
	}

	void WindowManagerSDL::freeWindow() {
		std::cout << "WindowManagerSDL: destroying window" << std::endl;
		SDL_DestroyWindow(window);
	}

	const SDL_Window *const WindowManagerSDL::getWindow() const {
		return window;
	}
}
