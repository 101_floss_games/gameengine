/**
 * system.cpp
 *
 * GameEngine
 *
 * 2022 Mohammad Rasmi Khashashneh
 *
 * MIT license
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "system.hpp"
#include <iostream>

namespace gameengine {

	System::~System() {}

	bool System::init(const WindowProperties &windowProperties) {
		try {
			if(!initSystem())
				return false;
		} catch(const std::exception &ex) {
			std::cout << "System ERROR: " << ex.what() << std::endl;
			return false;
		}

		if(!initWindowManager(windowProperties)) {
			freeSystem();
			return false;
		}

		if(!initRenderer()) {
			freeWindowManager();
			freeSystem();
			return false;
		}

		if(!initInput()) {
			freeRenderer();
			freeWindowManager();
			freeSystem();
			return false;
		}

		return true;
	}

	void System::free() {
		freeInput();
		freeRenderer();
		freeWindowManager();
		freeSystem();
	}

	const Input& System::getInputObject() const {
		const Input &ret = *input;
		return ret;
	}

	void System::render(const Scene *const scene) const {
		renderScene(scene);
	}
}
