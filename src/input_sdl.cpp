/**
* input_sdl.cpp
*
* GameEngine
*
* 2022 Mohammad Rasmi Khashashneh
*
* MIT license
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#include "input_sdl.hpp"
#include <iostream>

namespace gameengine {

	InputSDL::InputSDL() {}
	InputSDL::~InputSDL() {}

	const InputEvent InputSDL::getKeyboardEvent(
		const SDL_KeyboardEvent &event
	) const {
		switch(event.keysym.sym) {
			case SDLK_a:
			case SDLK_LEFT:
				return event.state == SDL_PRESSED ?
					InputEvent::INPUT_LEFT_PRESS :
					InputEvent::INPUT_LEFT_RELEASE;
			case SDLK_d:
			case SDLK_RIGHT:
				return event.state == SDL_PRESSED ?
					InputEvent::INPUT_RIGHT_PRESS :
					InputEvent::INPUT_RIGHT_RELEASE;
			case SDLK_w:
			case SDLK_UP:
				return event.state == SDL_PRESSED ?
					InputEvent::INPUT_UP_PRESS :
					InputEvent::INPUT_UP_RELEASE;
			case SDLK_s:
			case SDLK_DOWN:
				return event.state == SDL_PRESSED ?
					InputEvent::INPUT_DOWN_PRESS :
					InputEvent::INPUT_DOWN_RELEASE;
			case SDLK_z:
			case SDLK_SPACE:
				return event.state == SDL_PRESSED ?
					InputEvent::INPUT_A_PRESS :
					InputEvent::INPUT_A_RELEASE;
			case SDLK_x:
			case SDLK_LCTRL:
				return event.state == SDL_PRESSED ?
					InputEvent::INPUT_B_PRESS :
					InputEvent::INPUT_B_RELEASE;
			case SDLK_c:
				return event.state == SDL_PRESSED ?
					InputEvent::INPUT_X_PRESS :
					InputEvent::INPUT_X_RELEASE;
			case SDLK_v:
				return event.state == SDL_PRESSED ?
					InputEvent::INPUT_Y_PRESS :
					InputEvent::INPUT_Y_RELEASE;
			case SDLK_LSHIFT:
				return event.state == SDL_PRESSED ?
					InputEvent::INPUT_L_PRESS :
					InputEvent::INPUT_L_RELEASE;
			case SDLK_b:
				return event.state == SDL_PRESSED ?
					InputEvent::INPUT_R_PRESS :
					InputEvent::INPUT_R_RELEASE;
			case SDLK_RETURN:
				return event.state == SDL_PRESSED ?
					InputEvent::INPUT_START_PRESS :
					InputEvent::INPUT_START_RELEASE;
			case SDLK_RSHIFT:
				return event.state == SDL_PRESSED ?
					InputEvent::INPUT_SELECT_PRESS :
					InputEvent::INPUT_SELECT_RELEASE;
			default:
				return InputEvent::INPUT_NON;
		}
	}

	const InputEvent InputSDL::getInput() const {
		SDL_Event event; //TODO: confirm performance effect
		//TODO: send signals to input handlers here. return after no more events
		while(SDL_PollEvent(&event)) {
			switch(event.type) {
				case SDL_KEYDOWN: {
						// Add gamepad handlers
						InputEvent inputEvent = getKeyboardEvent(event.key);
						return inputEvent;
					}
				case SDL_QUIT: {
					return InputEvent::INPUT_QUIT;
				}
			}
		}
		return InputEvent::INPUT_NON;
	}

	void InputSDL::doInitialization() {}

	void InputSDL::doCleanup() {}

}
