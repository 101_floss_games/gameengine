/**
* windowmanager.hpp
*
* GameEngine
*
* 2022 Mohammad Rasmi Khashashneh
*
* MIT license
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include <cstdint>
#include <string>

namespace gameengine {
	#define DEFAULT_TITLE "Game Engine"

	struct WindowProperties {
		bool fullScreen;
		uint32_t width;
		uint32_t height;
		int x;
		int y;
		std::string title;

		WindowProperties(
			const std::string &windowTitle,
			bool fullScreen = false,
			uint32_t width = 1280, uint32_t height = 720,
			int x = -1, int y = -1
		);

		const WindowProperties& operator=(const WindowProperties& rvalue);
	};

	class WindowManager {
		public:
			void init(const WindowProperties &windowProperties);
			void close();
			virtual ~WindowManager();

		protected:
			WindowProperties windowProperties {DEFAULT_TITLE};

		private:
			virtual void createWindow() = 0;
			virtual void freeWindow() = 0;
	};
}
