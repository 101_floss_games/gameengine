/**
* input.hpp
*
* GameEngine
*
* 2022 Mohammad Rasmi Khashashneh
*
* MIT license
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#pragma once

namespace gameengine {

	enum class InputEvent {
		INPUT_NON,
		INPUT_LEFT_PRESS,
		INPUT_RIGHT_PRESS,
		INPUT_UP_PRESS,
		INPUT_DOWN_PRESS,
		INPUT_A_PRESS,
		INPUT_B_PRESS,
		INPUT_X_PRESS,
		INPUT_Y_PRESS,
		INPUT_L_PRESS,
		INPUT_R_PRESS,
		INPUT_START_PRESS,
		INPUT_SELECT_PRESS,
		INPUT_LEFT_RELEASE,
		INPUT_RIGHT_RELEASE,
		INPUT_UP_RELEASE,
		INPUT_DOWN_RELEASE,
		INPUT_A_RELEASE,
		INPUT_B_RELEASE,
		INPUT_X_RELEASE,
		INPUT_Y_RELEASE,
		INPUT_L_RELEASE,
		INPUT_R_RELEASE,
		INPUT_START_RELEASE,
		INPUT_SELECT_RELEASE,
		INPUT_QUIT
	};

	class Input {
		public:
			void init();
			void free();
			virtual ~Input();
			virtual const InputEvent getInput() const;

		private:
			virtual void doInitialization() = 0;
			virtual void doCleanup() = 0 ;
	};
}
