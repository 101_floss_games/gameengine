/**
* gameengine.hpp
*
* GameEngine
*
* 2022 Mohammad Rasmi Khashashneh
*
* MIT license
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "scene.hpp"
#include "system.hpp"
#include <chrono>

using namespace std::chrono_literals;

namespace gameengine {
	#define FPS_60 16.66666667ms

	enum class State {
		STOPPED,
		PAUSED,
		RUNNING
	};

	class GameEngine {
		private:
			Scene *currentScene;
			State engineState;
			System* system;
			WindowProperties windowProperties;

			virtual void runState();
			virtual bool init(const WindowProperties &windowProperties);
			virtual void free();

		public:
			GameEngine(System* system, const WindowProperties &windowProperties);
			virtual ~GameEngine();
			void setScene(Scene *scene);
			bool run();
			State getState() const;
	};
}
