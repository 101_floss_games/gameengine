/**
 * system.hpp
 *
 * GameEngine
 *
 * 2022 Mohammad Rasmi Khashashneh
 *
 * MIT license
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once
#include "renderer.hpp"
#include "input.hpp"
#include "windowmanager.hpp"
#include "scene.hpp"
#include <cstdint>

namespace gameengine {
	class System {
		public:
			virtual ~System() = 0;
			bool init(const WindowProperties &windowProperties);
			void render(const Scene* const scene) const;
			void free();
			const Input&  getInputObject() const;

		protected:
			//TODO: consider decoupling, provide accessors instead
			Renderer *renderer;
			Input *input;
			WindowManager *windowManager;

		private:
			virtual bool initSystem() = 0;
			virtual void freeSystem() = 0;
			virtual bool initRenderer() = 0;
			virtual void freeRenderer() = 0;
			virtual bool initInput() = 0;
			virtual void freeInput() = 0;
			virtual bool initWindowManager(const WindowProperties &windowProperties) = 0;
			virtual void freeWindowManager() = 0;
			virtual void renderScene(const Scene * const scene) const = 0;
	};
}
